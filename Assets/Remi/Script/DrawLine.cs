﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class DrawLine : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private float F_Dist;

    [Header("Transform")]
    [Tooltip("The orgine of the link")]
    public Transform T_Origin;
    [Tooltip("The destination of the link")]
    public Transform T_Destination;
    [Tooltip("Offset to the destination link")]
    public Vector3 V3_OffsetDestination;
    [Tooltip("Offset to the origin link")]
    public Vector3 V3_OffsetOrigin;

    [Header("Boolean")]
    [Tooltip("When a link are create with player")]
    public bool B_LinkedWithPlayer = false;
    [Tooltip("When a link are create with other caracter")]
    public bool B_LinkedWithOther = false;

    private void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetPosition(0,T_Origin.position + V3_OffsetOrigin);
        lineRenderer.SetWidth(0.10f,0.10f);

        F_Dist = Vector3.Distance(T_Origin.position,T_Destination.position);
    }

    private void Update()
    {
        F_Dist = Vector3.Distance(T_Origin.position, T_Destination.position);

        Vector3 pointA = T_Origin.position;
        Vector3 pointB = T_Destination.position;

        Vector3 pointAlongLine = F_Dist * Vector3.Normalize(pointB - pointA) + pointA;

        lineRenderer.SetPosition(1, pointAlongLine + V3_OffsetDestination);
        lineRenderer.SetPosition(0, T_Origin.position + V3_OffsetOrigin);
    }

    /// <summary>
    /// Destroy the line
    /// </summary>
    public void DestroyMyself()
    {
        Destroy(this.gameObject);
    }

}
