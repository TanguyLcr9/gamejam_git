﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class HouseState : MonoBehaviour
{

    public GameObject Go_HouseArea;
    public bool IsBuild;

    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    [Button]
    public void Attack()
    {
        if (IsBuild)
        {
            //attackthe building
            animator.SetTrigger("Destroy");
            IsBuild = false;
            AgentManager.Instance.ResetWicked();
        }
    }

    [Button]
    public void BuildHouse()
    {
        if (!IsBuild)
        {
            animator.SetTrigger("Build");
            IsBuild = true;
        }
    }
}
