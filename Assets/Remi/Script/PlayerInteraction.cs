﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    public GameObject G_CaracterToInteract;
    public float F_DistanceRequiredtoInteract = 10f;
    public GameObject G_WithWhy;
    public float F_BrakeDistance = 30f;
    public bool B_AreLinked = false;
    public Gradient Grad_Link;
    public Color Col_col;

    public AudioClip AC_CreateLink;
    public AudioClip AC_DestroyLink;

    private void Start()
    {
        Cursor.visible = false;
    }

    private void Update()
    {
        G_CaracterToInteract = AgentManager.Instance.AgentNextTo(transform);

        if (Input.GetButtonUp("Fire2"))
        {
            if (G_WithWhy != null )
            {
                if (GetComponent<LinkParameter>().B_haveLink == true)//couper lien avec le joueur
                {
                    G_CaracterToInteract.GetComponent<AreInteract>().HaveLink(null);
                    GetComponent<LinkParameter>().G_Instance.GetComponent<DrawLine>().DestroyMyself();
                    GetComponent<LinkParameter>().B_haveLink = false;
                    B_AreLinked = false;
                    GetComponent<AudioSource>().clip = AC_DestroyLink;
                    GetComponent<AudioSource>().Play();
                    G_WithWhy = null;
                }
            }
        }

        if (Vector3.Distance(transform.position,G_CaracterToInteract.transform.position) < F_DistanceRequiredtoInteract)
        {
            for (int i = 0; i < AgentManager.Instance.Agent_List.Count; i++)
            {
                AgentManager.Instance.Agent_List[i].GetComponent<AreInteract>().CanHaveInteraction(false);
            }

            G_CaracterToInteract.GetComponent<AreInteract>().CanHaveInteraction(true);      

            if (Input.GetButtonUp("Fire1"))
            {

                if (GetComponent<LinkParameter>().B_haveLink == false)// créer lien avec le joueur
                {
                    G_CaracterToInteract.GetComponent<AreInteract>().HaveLink(transform);
                    GetComponent<LinkParameter>().CreateLinkWithPlayer(G_CaracterToInteract.transform,transform);
                    G_WithWhy = G_CaracterToInteract;
                    B_AreLinked = true;
                    GetComponent<AudioSource>().clip = AC_CreateLink;
                    GetComponent<AudioSource>().Play();
                }
                else//Relier deux bonhommes
                {
                    if (AgentManager.Instance.IsMatch(G_WithWhy, G_CaracterToInteract))
                    {
                        G_WithWhy.GetComponent<AreInteract>().HaveLink(G_CaracterToInteract.transform);
                        G_CaracterToInteract.GetComponent<AreInteract>().HaveLink(G_WithWhy.transform);

                        G_WithWhy = null;

                        LinkParameter link = GetComponent<LinkParameter>();

                        link.G_Instance.GetComponent<DrawLine>().V3_OffsetOrigin = new Vector3(0, 0.8f, 0);
                        link.CreateLinkWithOther(G_CaracterToInteract.transform);
                        link.B_haveLink = false;
                        B_AreLinked = false;
                        GetComponent<AudioSource>().clip = AC_CreateLink;
                        GetComponent<AudioSource>().Play();
                        LineRenderer thisLine = GetComponent<LinkParameter>().G_Instance.GetComponent<LineRenderer>();
                        thisLine.endColor = Col_col;
                        thisLine.startColor = Col_col;
                        GetComponent<LinkParameter>().G_Instance = null;
                    }
                }                
            }

        }
        else 
        {
            G_CaracterToInteract.GetComponent<AreInteract>().CanHaveInteraction(false);
        }

        if (B_AreLinked == true && Vector3.Distance(transform.position,G_WithWhy.transform.position) > F_BrakeDistance) // casser le lien si il est trop loin
        {
            G_WithWhy.GetComponent<AreInteract>().HaveLink(null);
            GetComponent<LinkParameter>().G_Instance.GetComponent<DrawLine>().DestroyMyself();
            GetComponent<LinkParameter>().B_haveLink = false;
            B_AreLinked = false;
            GetComponent<AudioSource>().clip = AC_DestroyLink;
            GetComponent<AudioSource>().Play();
            G_WithWhy = null;
        }

        if (GetComponent<LinkParameter>().G_Instance != null && G_WithWhy != null)
        {
            LineRenderer thisLine = GetComponent<LinkParameter>().G_Instance.GetComponent<LineRenderer>();
            thisLine.endColor = Grad_Link.Evaluate(Vector3.Distance(transform.position, G_WithWhy.transform.position) / F_BrakeDistance);
            thisLine.startColor = Grad_Link.Evaluate(Vector3.Distance(transform.position, G_WithWhy.transform.position) / F_BrakeDistance);
        }
    }

    
}
