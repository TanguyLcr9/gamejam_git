﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using BBUnity.Conditions;

[Condition("LinkConditions/IsLink")]
[Help("Check Link")]
public class IsLink : GOCondition
{
    [OutParam("Target")]
    public GameObject target;

	public override bool Check()
	{
		AgentState agent = gameObject.GetComponent<AgentState>();
		if (agent.IsLink && agent.T_LinkObject != null)
		{
            target = agent.T_LinkObject.gameObject;
			return true;
		}
		return false;
	}
}