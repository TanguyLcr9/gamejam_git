﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxBehavior : MonoBehaviour
{
    public float tempsEnSecondes = 387f;
    float vitesseParSeconde;

    private void Start()
    {
        vitesseParSeconde = (tempsEnSecondes / 60f);
    }

    void Update()
    {
        transform.Rotate(vitesseParSeconde * Time.deltaTime,0,0);
    }
}
