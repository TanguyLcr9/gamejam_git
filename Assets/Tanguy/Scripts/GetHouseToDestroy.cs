﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*using Pada1.BBCore;           // Code attributes
using Pada1.BBCore.Tasks;     // TaskStatus
using Pada1.BBCore.Framework; // BasePrimitiveAction

[Action("Animation/GetHouseToDestroy")]
[Help("Attack next building")]
public class GetHouseToDestroy : BBUnity.Actions.GOAction
{

	

	public override TaskStatus OnUpdate()
	{

		//faire en sorte que le wander area devienne le Go_House , attention je pense qu'il faut aussi le reset après ça
		HouseToDestroy = HouseManager.Instance.GetHouseToDestroy();
		

		return TaskStatus.COMPLETED;
	}

}*/

using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using BBUnity.Conditions;

[Condition("LinkConditions/GetHouseToDestroy")]
[Help("Check House to destroy")]
public class GetHouseToDestroy : GOCondition
{
	[InParam("HouseToDestroyInput")]
	public GameObject HouseToDestroyInput;

	[OutParam("HouseToDestroy")]
	public GameObject HouseToDestroy;

	public override bool Check()
	{
        if (HouseToDestroyInput !=null)
        {
			return true;
        }
		HouseToDestroy = HouseManager.Instance.GetHouseToDestroy();
		return HouseToDestroy != null;
	}
}