﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using Pada1.BBCore;           // Code attributes
using Pada1.BBCore.Tasks;     // TaskStatus
using Pada1.BBCore.Framework; // BasePrimitiveAction

[Action("Animation/DoParticle")]
[Help("Particle")]
public class DoParticle : BBUnity.Actions.GOAction
{

	[InParam("Type")]
	public int Type;

	public override TaskStatus OnUpdate()
	{

		//get the comp of the house to damage it
		gameObject.GetComponent<AgentState>().DoParticle(Type);

		return TaskStatus.COMPLETED;
	}

}