﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using BBUnity.Conditions;

[Condition("LinkConditions/IsWicked")]
[Help("Check Link")]
public class IsWicked : GOCondition
{

	public override bool Check()
	{
		AgentState agent = gameObject.GetComponent<AgentState>();
		if (agent.IsWicked)
		{
			return true;
		}
		return false;
	}
}