﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AgentManager : MonoBehaviour
{
    public static AgentManager Instance;

    //prefabs des Lambdas
    public GameObject Agent;
    //nombres des Lambdas
    public int Nombre;

    //nombre d'ability dispo
    public const int nbUI = 7;

    //espacement au spawn
    public float Range = 20;

    //Area de deplacement des Lambda
    public GameObject WanderArea;
    //list des Lambda
    public List<GameObject> Agent_List;

    //list de sprite 
    public Sprite[] spriteTrue = new Sprite[7];
    public Sprite[] spriteFalse = new Sprite[7];

    float timerUntilWicked;
    float timeMax;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        //nombre pair d'agents Lambdas
        Nombre *= 2;
        for (int i = 0; i < Nombre; i++)
        {
            Vector3 ranPos = new Vector3(Random.Range(-Range, Range),0f, Random.Range(-Range, Range));
            GameObject newAgent = Instantiate(Agent, ranPos, Quaternion.identity);
            newAgent.GetComponent<BehaviorExecutor>().SetBehaviorParam("WanderArea", WanderArea);
            newAgent.transform.SetParent(transform);
            Agent_List.Add(newAgent);
            newAgent.gameObject.name = "Agent " + i;
        }

        //apply Ability
        ApplyAbility();

        //apply UI en fonction du numero et bool
        ApplyUI();

        //randomTimerMax
        timeMax = Random.Range(30f,50f);
    }

    // Update is called once per frame
    void Update()
    {
        timerUntilWicked += Time.deltaTime;
        if (timerUntilWicked > timeMax)
        {
            int ran = Random.Range(0, Agent_List.Count);
            Agent_List[ran].GetComponent<AgentState>().CheckIsWicked();
            
            timeMax = Random.Range(30f, 50f);
            timerUntilWicked = 0;
            
        }

    }


    public GameObject AgentNextTo(Transform target)
    {
        GameObject AgentProxi = Agent_List[0];
        for (int i = 0; i < Agent_List.Count; i++)
        {
            if (Vector3.Distance(target.position, Agent_List[i].transform.position) < Vector3.Distance(target.position, AgentProxi.transform.position))
            {
                AgentProxi = Agent_List[i];
            }
        }
        return AgentProxi;
    }

    //application d une capacite a chaque agent lambda
    public void ApplyAbility()
    {
        int compteur = 0;
        foreach(GameObject agent in Agent_List)
        {
            agent.GetComponent<LambdaAbilities>().ability = (int) compteur / 2;
        
            agent.GetComponent<LambdaAbilities>().isRequireAbility = (compteur % 2 == 0);
            compteur++;
            compteur %= nbUI *2 ;
        }
    }

    /// <summary>
    ///  pour eviter qu'il restent sur le meme building
    /// </summary>
    public void ResetWicked()
    {
        for (int i = 0; i < Agent_List.Count; i++) {
            if (Agent_List[i].GetComponent<AgentState>().IsWicked)
            {
                Agent_List[i].GetComponent<AgentState>().Go_House = null;
            }
          }
    }

    //
    public void ApplyUI()
    {
        foreach (GameObject agent in Agent_List)
        {
            int abilityAgent = agent.GetComponent<LambdaAbilities>().ability;
            bool requireAbility = agent.GetComponent<LambdaAbilities>().isRequireAbility;
            switch (abilityAgent)
            {
                case 0:
                    if (requireAbility)
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteTrue[abilityAgent];
                    }
                    else
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteFalse[abilityAgent];
                    }
                    break;
                case 1:
                    if (requireAbility)
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteTrue[abilityAgent];
                    }
                    else
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteFalse[abilityAgent];
                    }
                    break;
                case 2:
                    if (requireAbility)
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteTrue[abilityAgent];
                    }
                    else
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteFalse[abilityAgent];
                    }
                    break;
                case 3:
                    if (requireAbility)
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteTrue[abilityAgent];
                    }
                    else
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteFalse[abilityAgent];
                    }
                    break;
                case 4:
                    if (requireAbility)
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteTrue[abilityAgent];
                    }
                    else
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteFalse[abilityAgent];
                    }
                    break;
                case 5:
                    if (requireAbility)
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteTrue[abilityAgent];
                    }
                    else
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteFalse[abilityAgent];
                    }
                    break;
                case 6:
                    if (requireAbility)
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteTrue[abilityAgent];
                    }
                    else
                    {
                        agent.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = spriteFalse[abilityAgent];
                    }
                    break;
                default:
                    Debug.Log("Error! ability not valid");
                    break;
            }
        }
    }

    //renvoi si les liens matchs
    public bool IsMatch(GameObject agent1, GameObject agent2)
    {
        if (agent1.GetComponent<LambdaAbilities>().ability == agent2.GetComponent<LambdaAbilities>().ability)
        {
            return (agent1.GetComponent<LambdaAbilities>().isRequireAbility != agent2.GetComponent<LambdaAbilities>().isRequireAbility);
        }
        else
        {
            return false;
        }
    }


}
