﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreInteract : MonoBehaviour
{
    public Color C_InteractionColor;
    public Color C_BaseColor;
    private float F_WaitTime;

    private void Start()
    {
        transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.color = C_BaseColor;
        transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.SetColor("_EmissionColor", C_BaseColor);
        transform.GetChild(1).GetChild(0).gameObject.SetActive(false);
    }

    public void CanHaveInteraction(bool B_CanInteract)
    {
        if (B_CanInteract == true)
        {
            transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.color = C_InteractionColor;
            transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.SetColor("_EmissionColor", C_InteractionColor);
            transform.GetChild(1).GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.color = C_BaseColor;
            transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.SetColor("_EmissionColor", C_BaseColor);
            transform.GetChild(1).GetChild(0).gameObject.SetActive(false);
        }

    }

    public void HaveLink(Transform T_Target)
    {
        GetComponent<AgentState>().T_LinkObject = T_Target;
        GetComponent<AgentState>().IsLink = true;
    }
}
