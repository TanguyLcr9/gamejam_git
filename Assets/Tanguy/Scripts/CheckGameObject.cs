﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using BBUnity.Conditions;

[Condition("LinkConditions/CheckGameObject")]
[Help("Check Link")]
public class CheckGameObject : GOCondition
{
    [InParam("GameObjectToCheck")]
    public GameObject GameObjectToCheck;

	public override bool Check()
	{
		Debug.Log(GameObjectToCheck.name+" ");
		return GameObjectToCheck == null;
	}
}