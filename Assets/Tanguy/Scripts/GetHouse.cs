﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using Pada1.BBCore;           // Code attributes
using Pada1.BBCore.Tasks;     // TaskStatus
using Pada1.BBCore.Framework; // BasePrimitiveAction

[Action("Animation/GetHouse")]
[Help("Attack next building")]
public class GetHouse : BBUnity.Actions.GOAction
{

	[OutParam("HouseTarget")]
	public GameObject House;

	public override TaskStatus OnUpdate()
	{
		AgentState agent = gameObject.GetComponent<AgentState>();

		//faire en sorte que le wander area devienne le Go_House , attention je pense qu'il faut aussi le reset après ça
		//agent.Go_House = HouseManager.Instance.GetHouseToBuild(gameObject.transform);

		House = agent.Go_House.GetComponent<HouseState>().Go_HouseArea;

		return TaskStatus.COMPLETED;
	}

}