﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseManager : MonoBehaviour
{
    public static HouseManager Instance;

    public List<GameObject> Houses_List;

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        for(int i=0; i < transform.childCount; i++)
        {
            Houses_List.Add(transform.GetChild(i).gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Retourne la maison la plus proche
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public GameObject HouseNextTo(Transform target)
    {
        GameObject HouseProxi = Houses_List[0];
        for(int i = 0; i < Houses_List.Count; i++)
        {
            if (Vector3.Distance(target.position, Houses_List[i].transform.position) < Vector3.Distance(target.position, HouseProxi.transform.position))
                HouseProxi = Houses_List[i];
        }
        return HouseProxi;
    }

    /// <summary>
    /// Build or destroy selected house next to lambdas
    /// </summary>
    /// <param name="target"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    public GameObject GetHouseToBuild(Transform target)
    {
        //GameObject HouseProxi = Houses_List[0];
        //for (int i = 0; i < Houses_List.Count; i++)
        //{

        //    if (Vector3.Distance(target.position, Houses_List[i].transform.position)
        //        < Vector3.Distance(target.position,  HouseProxi.transform.position ))
        //    { 
        //        if (Houses_List[i].GetComponent<HouseState>().IsBuild == false)
        //        {
        //            HouseProxi = Houses_List[i];
        //        }
        //    }
        //}

        
            GameObject HouseProxi = Houses_List[Random.Range(0, Houses_List.Count)];
        
        return HouseProxi;
    }

    public GameObject GetHouseToDestroy()
    {
        int ran = Random.Range(0, Houses_List.Count);
        if (Houses_List[ran].GetComponent<HouseState>().IsBuild)
        {
            return Houses_List[ran];
        }
        return null;
    }

}
