﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class LinkParameter : MonoBehaviour
{

    [Header("Object")]
    [Tooltip("The link instance")]
    public GameObject G_LinkObject;
    public GameObject G_Instance;
    public bool B_haveLink = false;

    /// <summary>
    /// Create a link with the player and the other caracter selectioned
    /// </summary>
    /// <param name="T_Destination"></param>
    /// <param name="T_Origin"></param>
    public void CreateLinkWithPlayer(Transform T_Destination, Transform T_Origin)
    {
        B_haveLink = true;
        G_Instance = Instantiate(G_LinkObject,Vector3.zero,Quaternion.identity);
        G_Instance.GetComponent<DrawLine>().T_Origin = T_Origin;
        G_Instance.GetComponent<DrawLine>().T_Destination = T_Destination;
        G_Instance.GetComponent<DrawLine>().B_LinkedWithPlayer = true;
        G_Instance.GetComponent<DrawLine>().B_LinkedWithOther = false;
    }

    /// <summary>
    /// Change the origine of the last link to the new carater selectioned
    /// </summary>
    /// <param name="T_NewOrigin"></param>
    public void CreateLinkWithOther(Transform T_NewOrigin)
    {
        G_Instance.GetComponent<DrawLine>().T_Origin = T_NewOrigin;
        G_Instance.GetComponent<DrawLine>().B_LinkedWithPlayer = false;
        G_Instance.GetComponent<DrawLine>().B_LinkedWithOther = true;
    }

}
