﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentState : MonoBehaviour
{
    public bool IsLink;
    public bool IsWicked;

    public Transform T_LinkObject;
    public GameObject Go_House;

    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (IsLink && IsWicked)
        {
            IsWicked = false;
            Go_House = null;
            transform.GetChild(0).GetComponent<Renderer>().material.color = Color.black;
        }

        
    }

    /// <summary>
    /// le rend wicked si n'est pas link
    /// </summary>
   public void CheckIsWicked()
    {
        if (!IsLink)
        {
            IsWicked = true;
            StartCoroutine(Wicked());
        }
        
    }

    /// <summary>
    /// wich particule is set
    /// </summary>
    /// <param name="type"></param>
    public void DoParticle(int type)
    {
        switch (type)
        {
            case 0:
                if(!GetComponent<LambdaAbilities>().couerPart.isPlaying)
                GetComponent<LambdaAbilities>().PlayCoeur();
                break;

            case 1:
                if (!GetComponent<LambdaAbilities>().couerPart.isPlaying)
                    GetComponent<LambdaAbilities>().PlayCross();
                break;

        }

    }

    public void SetAnimation(int type)
    {
        transform.GetChild(0).GetComponent<Animator>().SetBool("Happy", false);
        transform.GetChild(0).GetComponent<Animator>().SetBool("Angry", false);
        transform.GetChild(0).GetComponent<Animator>().SetBool("Normal", false);
        transform.GetChild(0).GetComponent<Animator>().SetBool("Sad", false);
        transform.GetChild(0).GetComponent<Animator>().SetBool("Destroy", false);
        transform.GetChild(0).GetComponent<Animator>().SetBool("Exited", false);
        transform.GetChild(0).GetComponent<Animator>().SetBool("Idle", false);
        switch (type)
        {
            case 0:
                transform.GetChild(0).GetComponent<Animator>().SetBool("Happy", true);
                break;
            case 1:
                transform.GetChild(0).GetComponent<Animator>().SetBool("Angry", true);
                break;
            case 2:
                transform.GetChild(0).GetComponent<Animator>().SetBool("Normal", true);
                break;
            case 3:
                transform.GetChild(0).GetComponent<Animator>().SetBool("Sad", true);
                break;
            case 4:
                transform.GetChild(0).GetComponent<Animator>().SetBool("Destroy", true);
                break;
            case 5:
                transform.GetChild(0).GetComponent<Animator>().SetBool("Exited", true);
                break;
                case 6:
                transform.GetChild(0).GetComponent<Animator>().SetBool("Idle", true);
                break;
        }

    }

    IEnumerator Wicked()
    {
        while (IsWicked)
        {
            Go_House = HouseManager.Instance.GetHouseToDestroy();
            //Debug.Log("this " + Go_House.name);
            yield return new WaitForSeconds(2f);
        }

    }
}
