﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using BBUnity.Conditions;

[Condition("LinkConditions/HaveHouse")]
[Help("Check Link")]
public class HaveHouse : GOCondition
{
	[OutParam("House")]
	public GameObject House;

	public override bool Check()
	{
		AgentState agent = gameObject.GetComponent<AgentState>();

			return agent.Go_House != null;
	}
}