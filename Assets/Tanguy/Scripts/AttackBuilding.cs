﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using Pada1.BBCore;           // Code attributes
using Pada1.BBCore.Tasks;     // TaskStatus
using Pada1.BBCore.Framework; // BasePrimitiveAction

[Action("Animation/AttackBuilding")]
[Help("Attack next building")]
public class AttackBuilding : BBUnity.Actions.GOAction
{
	[InParam("House")]
	public GameObject House;

	public override TaskStatus OnUpdate()
	{

		//get the comp of the house to damage it
		House.GetComponent<HouseState>().Attack();

		return TaskStatus.COMPLETED;
	}

}