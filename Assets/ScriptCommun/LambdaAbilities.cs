﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class LambdaAbilities : MonoBehaviour
{
    //numero de la capacite, de 0 a 7
    public int ability;

    //boolean, true demande ability sinon a ability
    public bool isRequireAbility;

    public ParticleSystem couerPart;
    public ParticleSystem crossPart;
    
    [Button]
    public void PlayCoeur()
    {
        couerPart.Emit(10);
    }

    [Button]
    public void PlayCross()
    {
        crossPart.Emit(30);
    }
}
