﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerBehavior : MonoBehaviour
{
    [Header("Stats")]
    //vitesse de déplacement et rotation
    public float moveSpeed;
    public float rotateSpeed;
    public const float minZoom = 10f, maxZoom = 20f;
    [Range(minZoom, maxZoom)]
    public float zoomVal;

    //Inputs
    private float horInput, vertInput;
    private Vector3 movDir;
    private Vector3 turnDir;

    [Header("J'ai pas d'idée là")]
    //Rigidbody
    public Rigidbody rigid;
    //camera principale
    public Transform cameraGo;
    [HideInInspector]
    public CinemachineFreeLook freeLookCam;
    [HideInInspector]
    public CinemachineFreeLook.Orbit[] originalOrbits;

    public Animator anim;


    //variables de deplacement 
    [Header("Inputs")]
    public float vertical;
    public float horizontal;
    [HideInInspector]
    public float moveAmountVerti;
    [HideInInspector]
    public float moveAmountHori;
    [HideInInspector]
    public float turnAmount = 1f;
    [HideInInspector]
    public int backMove;
    [HideInInspector]
    public int leftMove;
    [HideInInspector]
    public Vector3 moveDirVerti;
    [HideInInspector]
    public Vector3 moveDirHori;
    [HideInInspector]
    public Vector3 desiredLookRotation;
    private float prevCursorPosition;
    private bool lastInputAxisState;

    // Start is called before the first frame update
    void Start()
    {
        //initialisation du deplacement
        movDir = new Vector3();

        //initialisation du rigidBoby et constrains 
        rigid = gameObject.GetComponent<Rigidbody>();
        rigid.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        rigid.constraints = RigidbodyConstraints.FreezeRotation;

        //initialisation de la camera
        cameraGo = GameObject.FindGameObjectWithTag("MainCamera").transform;

        
    }


    // Update is called once per frame
    void Update()
    {
        //update des variables d'Inputs
        bool Inputs = Input.GetButtonUp("Horizontal") || Input.GetButtonUp("Vertical");
        //Debug.Log(Inputs);
        
        GetAxis();
        if (GetComponent<Rigidbody>().velocity != Vector3.zero)
        {
            anim.SetBool("Walk", true);
            anim.SetBool("Idle", false);
        }
        else
        {
            anim.SetBool("Idle",true);
            anim.SetBool("Walk", false);
        }
    }

    // LateUpdate is called once per frame after the Update method
    void FixedUpdate()
    {
        //modification des etats de deplacements
        UpdateStates();
        //appel de l equivalent FixedUpdate sur PlayerStates
        FixedTick(Time.fixedDeltaTime);
    }

    // ############################# Fonctions annexes ######################################## //

    //accesseur des Inputs
    void GetAxis()
    {
        //assignation des imputs aux variables
        vertical = Input.GetAxis("Vertical");
        horizontal = Input.GetAxis("Horizontal");
    }

    //en fonction des Inputs les variables d'etat et celle de calcul de deplacement sont modifier
    void UpdateStates()
    {
        

        ///////////////    deplacement relatif camera    ////////////////////////////////////////////
        ////////////////////////////////////////////////  deplacement
        /////////////// pre_calcul & re_init

        //acces a la position de la camera
        Transform cameraTransf = cameraGo.transform;

        //calcul de variables de camera
        Vector3 forwardCam = cameraTransf.forward;
        Vector3 rightCam = cameraTransf.right;


        Vector3 camDir = cameraTransf.position - transform.position;
        camDir = transform.InverseTransformDirection(camDir);
        Debug.DrawRay(transform.position, camDir, Color.white);


        ///////////////////////////////mouvement vertical & horizontal
        //direction de mouvement vertical
        moveDirVerti = -camDir;
        moveDirVerti.y = 0;
        moveDirVerti.Normalize();
        Debug.DrawRay(transform.position, moveDirVerti * 50f, Color.white);

        //deduction du mouvement horizontal
        float _x = moveDirVerti.x;
        float _z = moveDirVerti.z;
        float PI_half = Mathf.PI / 2;
        float px = _x * Mathf.Cos(PI_half) - _z * Mathf.Sin(PI_half);
        float pz = _x * Mathf.Sin(PI_half) + _z * Mathf.Cos(PI_half);
        //assignation
        Vector3 moveDirRight = Vector3.zero;
        moveDirRight.x = px;
        moveDirRight.z = pz;
        moveDirHori = moveDirRight;

        //sens de mouvement vertical
        if (vertical < 0)
        {
            backMove = -1;
        }
        else
        {
            backMove = 1;
        }

        //sens de mouvement horizontal
        if (horizontal < 0)
        {
            leftMove = 1;
        }
        else
        {
            leftMove = -1;
        }

        //vitesse de mouvement vertical
        float absMVert = Mathf.Abs(vertical);
        float absMHori = Mathf.Abs(horizontal);
        moveAmountVerti = Mathf.Clamp01(absMVert);
        moveAmountHori = Mathf.Clamp01(absMHori);



        ///////////////////////////////////////// rotation
        Vector3 forwardPos = transform.forward;
        Vector3 CamPos = cameraTransf.position - transform.position;
        Vector3 desiredRotation = Vector3.Slerp(forwardPos, CamPos, 0.5f);
        desiredRotation = transform.InverseTransformDirection(desiredRotation);
        desiredRotation.y = 0f;

        //Debug.DrawRay(transform.position, CamPos, Color.yellow);
        Debug.DrawRay(transform.position, desiredRotation, Color.green);

        turnDir = movDir;


    }

    public void FixedTick(float d)
    {

        //deplacement du joueur aux positions calculees   //position avant - arriere
        movDir = (moveDirVerti * backMove * moveAmountVerti) + (moveDirHori * leftMove * moveAmountHori);
        Debug.DrawRay(transform.position, movDir * 10f, Color.red);
        transform.Translate(movDir * moveSpeed * Time.fixedDeltaTime);

        //rotation du joueur en fonction de sa direction
        float AutoRotateForce = turnDir.x;
        Vector3 AutoRotation = new Vector3(0f, AutoRotateForce * rotateSpeed * Time.fixedDeltaTime, 0f);
        transform.Rotate(AutoRotation);
    }

    private bool GetAxisInputLikeGetKeyDown(string axisName)
    {
        bool currentInputValue = (Input.GetAxis(axisName) > 0.1);

        //previent contre le maintien de bouton
        if (currentInputValue && lastInputAxisState)
        {
            return false;
        }

        lastInputAxisState = currentInputValue;

        return currentInputValue;
    }
}