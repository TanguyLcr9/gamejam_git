﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using BBUnity.Conditions;

[Condition("LinkConditions/IsLinkToPlayer")]
[Help("Check Link")]
public class IsLinkToPlayer : GOCondition
{

	public override bool Check()
	{
		AgentState agent = gameObject.GetComponent<AgentState>();
		if (agent.T_LinkObject.gameObject.CompareTag("Player")) 
		{
			return true;
		}
		return false;
	}
}