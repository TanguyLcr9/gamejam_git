﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using Pada1.BBCore;           // Code attributes
using Pada1.BBCore.Tasks;     // TaskStatus
using Pada1.BBCore.Framework; // BasePrimitiveAction

[Action("Animation/GetHouseToBuild")]
[Help("Attack next building")]
public class GetHouseToBuild : BBUnity.Actions.GOAction
{

	[OutParam("HouseToBuild")]
	public GameObject HouseToBuild;

	public override TaskStatus OnUpdate()
	{
		AgentState agent = gameObject.GetComponent<AgentState>();

		//faire en sorte que le wander area devienne le Go_House , attention je pense qu'il faut aussi le reset après ça
		HouseToBuild = HouseManager.Instance.GetHouseToBuild(agent.transform);
		

		return TaskStatus.COMPLETED;
	}

}