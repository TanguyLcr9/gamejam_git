﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NaughtyAttributes;

public class DisplayText : MonoBehaviour
{ 
public TextMeshProUGUI textDisplay;
public string[] sentences;
private int index;
public float typingSpeed;
private float currentTypingSpeed;
private bool reading = false;
private bool WaitButton = false;
private bool finish = false;

private void Start()
{
    currentTypingSpeed = typingSpeed;
}

void Update()
{
    if (reading == false)
    {
        StartCoroutine(Type());
        reading = true;
    }

    if (textDisplay.text == sentences[index] && WaitButton == false && finish == false)
    {
            GetComponent<AudioSource>().Stop();
            StartCoroutine("Button");
        WaitButton = true;
    }
}

IEnumerator Button()
{
    yield return new WaitForSeconds(1f);
        NextSentence();
}

    IEnumerator Type()
    {
        
        yield return new WaitForSeconds(0.5f);
        GetComponent<AudioSource>().Play();
        foreach (char letter in sentences[index].ToCharArray())
        {
            string textL = letter+ "";
            if (textL == " ")
            {
                GetComponent<AudioSource>().Stop();
            }
            else
            {
                GetComponent<AudioSource>().Play();
            }
            textDisplay.text += letter;
            yield return new WaitForSeconds(currentTypingSpeed);
        }
    }

public void NextSentence()
{
    StartCoroutine("nextSentence");
    currentTypingSpeed = typingSpeed;
}

IEnumerator nextSentence()
{
    yield return new WaitForSeconds(0.75f);
    WaitButton = false;

    if (index < sentences.Length - 1 && finish == false)
    {
        index++;
        textDisplay.text = "";
        StartCoroutine(Type());
    }
    else if (finish == false)
    {
        textDisplay.text = "";
        gameObject.SetActive(false);
    }
}

}
