﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILookAtCamera : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Camera.main.transform);
        Debug.DrawRay(transform.position, Camera.main.transform.position,Color.magenta);
        Debug.Log(Vector3.Distance(transform.position, Camera.main.transform.position));
    }
}
