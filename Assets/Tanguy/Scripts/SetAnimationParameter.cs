﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


using Pada1.BBCore;           // Code attributes
using Pada1.BBCore.Tasks;     // TaskStatus
using Pada1.BBCore.Framework; // BasePrimitiveAction

[Action("Animation/SetAnimationParameter")]
[Help("Set anim & speed")]
public class SetAnimationParameter : BBUnity.Actions.GOAction
{

	[InParam("Type")]
	public int Type;


	[InParam("NewSpeed")]
	public float Speed = 1;

	public override TaskStatus OnUpdate()
	{

		//get the comp of the house to damage it
		gameObject.GetComponent<AgentState>().SetAnimation(Type);
		gameObject.GetComponent<NavMeshAgent>().speed = Speed;

		return TaskStatus.COMPLETED;
	}

}