﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRandomSound : MonoBehaviour
{
    public AudioClip[] AC_SoundBlaBla;
    public float F_TimeMin;
    public float F_TimeMax;
    public float F_WaitTime;

    private void Start()
    {
        F_WaitTime = Random.Range(F_TimeMin, F_TimeMax);
    }


    private void Update()
    {
        if (F_WaitTime <=0)
        {
            GetComponent<AudioSource>().clip = AC_SoundBlaBla[Random.Range(0, AC_SoundBlaBla.Length)];
            GetComponent<AudioSource>().Play();

            F_WaitTime = Random.Range(F_TimeMin, F_TimeMax);
        }
        else
        {
            F_WaitTime -= Time.deltaTime;
        }
    }

}
