﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotate : MonoBehaviour
{
    private void Start()
    {
        transform.rotation = Quaternion.Euler(0,Random.Range(0,360),0);
    }
}
