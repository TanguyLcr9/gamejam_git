﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomScale : MonoBehaviour
{
    public float F_ScaleMax;
    public float F_ScaleMin;

    private void Start()
    {
        float F_Value = Random.Range(F_ScaleMin,F_ScaleMax);
        transform.localScale = new Vector3(F_Value, F_Value, F_Value);
    }
}
